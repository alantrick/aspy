import json
from collections import OrderedDict
import datetime
import isodate.tzinfo

import activitystreams2

# json comparing helper functions, see
# https://stackoverflow.com/questions/25851183/


def ordered(obj):
    if isinstance(obj, dict):
        return OrderedDict(sorted((k, ordered(v)) for k, v in obj.items()))
    if isinstance(obj, list):
        return [ordered(x) for x in obj]
    else:
        return obj


def assert_deep_equals(first, second, prefix=""):
    if isinstance(first, OrderedDict) and isinstance(second, OrderedDict):
        for key, value in first.items():
            if key in second:
                assert_deep_equals(value, second.pop(key))
            else:
                assert False, "Missing key in second{}: {}".format(
                    " at " + prefix if prefix else "", key
                )
        assert len(second) < 1, "Missing key in first{}: {}".format(
            " at " + prefix if prefix else "", second.popitem()[0]
        )
    elif isinstance(first, OrderedDict) or isinstance(second, OrderedDict):
        assert False, "Type mismatch{}: {} != {}".format(
            " at " + prefix if prefix else "", first, second
        )
    else:
        assert first == second, "{} != {}{}".format(
            first, second, " at " + prefix if prefix else ""
        )


def assert_json_equals(str_first, str_second):
    first = json.loads(str_first)
    second = json.loads(str_second)
    assert_deep_equals(ordered(first), ordered(second))
    assert ordered(first) == ordered(second)


class TestObjects:
    """Really small tests to make sure basic things work as expected."""

    def test_alter(self):
        """Make sure you can alter an object that isn't in immutable mode"""

        obj = activitystreams2.Article(id="http://example.org/why-immutability-sucks")
        name = "Immutability considered harmful"
        obj["name"] = name
        expected = json.dumps(
            {
                "@context": "https://www.w3.org/ns/activitystreams",
                "type": "Article",
                "name": "Immutability considered harmful",
                "id": "http://example.org/why-immutability-sucks",
            },
            indent=2,
        )
        # check the get version
        assert name == obj["name"]
        assert_json_equals(expected, str(obj))

    def test_no_alter(self):
        """Make sure you can't alter an object that is in immutable mode"""

        obj = activitystreams2.Article(id="http://example.org/why-mutability-sucks")
        obj.set_immutable()
        name = "Mutability considered harmful"
        try:
            obj["name"] = name
        except TypeError:
            pass
        else:
            assert False, "Mutated object that should be immutable"

    def test_contains(self):
        obj = activitystreams2.Article(id="http://example.org/")
        assert "id" in obj

    def test_image(self):
        image = activitystreams2.Image(
            name="A picture of my cat", url="http://example.org/img/cat.png"
        )
        expected = json.dumps(
            {
                "@context": "https://www.w3.org/ns/activitystreams",
                "type": "Image",
                "name": "A picture of my cat",
                "url": "http://example.org/img/cat.png",
            },
            indent=2,
        )
        assert_json_equals(expected, str(image))

    def test_image_with_fallback(self):
        image = activitystreams2.Image(
            name="Cat Jumping on Wagon",
            url=[
                activitystreams2.Link(
                    href="http://example.org/image.jpeg", media_type="image/jpeg"
                ),
                activitystreams2.Link(
                    href="http://example.org/image.png", media_type="image/png"
                ),
            ],
        )
        expected = json.dumps(
            {
                "@context": "https://www.w3.org/ns/activitystreams",
                "type": "Image",
                "name": "Cat Jumping on Wagon",
                "url": [
                    {
                        "type": "Link",
                        "href": "http://example.org/image.jpeg",
                        "mediaType": "image/jpeg",
                    },
                    {
                        "type": "Link",
                        "href": "http://example.org/image.png",
                        "mediaType": "image/png",
                    },
                ],
            },
            indent=2,
        )
        assert_json_equals(expected, str(image))

    def test_collection(self):

        collection = activitystreams2.Collection(
            summary="Sally's notes",
            items=[activitystreams2.Note(name="Which Staircase Should I Use")],
        )

        expected = json.dumps(
            {
                "@context": "https://www.w3.org/ns/activitystreams",
                "summary": "Sally's notes",
                "type": "Collection",
                "totalItems": 1,
                "items": [{"type": "Note", "name": "Which Staircase Should I Use"}],
            }
        )
        assert_json_equals(expected, str(collection))
        collection["items"].append(activitystreams2.Note(name="Something to Remember"))

        expected = json.dumps(
            {
                "@context": "https://www.w3.org/ns/activitystreams",
                "summary": "Sally's notes",
                "type": "Collection",
                "totalItems": 2,
                "items": [
                    {"type": "Note", "name": "Which Staircase Should I Use"},
                    {"type": "Note", "name": "Something to Remember"},
                ],
            }
        )
        assert_json_equals(expected, str(collection))


class TestTypes:
    def test_event(self):
        sally = activitystreams2.Person(name="Sally")
        john = activitystreams2.Object(id="http://john.example.org")
        event = activitystreams2.Event(name="Going-Away Party for Jim")
        invite = activitystreams2.Invite(actor=john, object=event)
        activity = activitystreams2.Accept(
            summary="Sally accepted an invitation to a party",
            actor=sally,
            object=invite,
        )
        expected = (
            "{\n"
            '  "@context": '
            '"https://www.w3.org/ns/activitystreams",\n'
            '  "summary": '
            '"Sally accepted an invitation to a party",\n'
            '  "type": "Accept",'
            '  "actor": {'
            '    "type": "Person",'
            '    "name": "Sally"'
            "  },"
            '  "object": {'
            '    "type": "Invite",'
            '    "actor": "http://john.example.org",'
            '    "object": {'
            '      "type": "Event",'
            '      "name": "Going-Away Party for Jim"'
            "    }"
            "  }"
            "}"
        )
        assert_json_equals(expected, str(activity))

    def test_add(self):
        sally = activitystreams2.Person(name="Sally")
        image = activitystreams2.Image(
            name="A picture of my cat", url="http://example.org/img/cat.png"
        )
        origin = activitystreams2.Collection(name="Camera Roll")
        target = activitystreams2.Collection(name="My Cat Pictures")
        activity = activitystreams2.Add(
            summary="Sally added a picture of her cat to her cat picture " "collection",
            actor=sally,
            object=image,
            origin=origin,
            target=target,
        )

        expected = json.dumps(
            {
                "@context": "https://www.w3.org/ns/activitystreams",
                "summary": "Sally added a picture of her cat to her cat "
                "picture collection",
                "type": "Add",
                "actor": {"type": "Person", "name": "Sally"},
                "object": {
                    "type": "Image",
                    "name": "A picture of my cat",
                    "url": "http://example.org/img/cat.png",
                },
                "origin": {"type": "Collection", "name": "Camera Roll"},
                "target": {"type": "Collection", "name": "My Cat Pictures"},
            },
            indent=2,
        )
        assert_json_equals(expected, str(activity))


class TestActivityPub:
    def test_actor(self):
        url = "https://sally.name/"
        sally = activitystreams2.Person(
            id=url,
            name="Sally",
            preferred_username="sally",
            liked=url + "liked",
            likes=url + "likes",
            followers=url + "followers",
            following=url + "following",
            inbox=url + "inbox",
            outbox=url + "outbox",
        )
        expected = json.dumps(
            {
                "@context": "https://www.w3.org/ns/activitystreams",
                "id": "https://sally.name/",
                "type": "Person",
                "name": "Sally",
                "preferredUsername": "sally",
                "liked": "https://sally.name/liked",
                "likes": "https://sally.name/likes",
                "followers": "https://sally.name/followers",
                "following": "https://sally.name/following",
                "inbox": "https://sally.name/inbox",
                "outbox": "https://sally.name/outbox",
            },
            indent=2,
        )
        assert_json_equals(expected, str(sally))


class TestExtension:
    def test_read(self):
        message = activitystreams2.Note(
            id="https://sally.name/notes/1", content="foobar"
        )
        message["https://schema.org/dateRead"] = datetime.datetime(
            2018, 10, 25, 5, 11, 2, tzinfo=isodate.tzinfo.UTC
        )
        expected = json.dumps(
            {
                "@context": [
                    "https://www.w3.org/ns/activitystreams",
                    {"dateRead": "https://schema.org/dateRead"},
                ],
                "type": "Note",
                "id": "https://sally.name/notes/1",
                "content": "foobar",
                "dateRead": "2018-10-25T05:11:02Z",
            },
            indent=2,
        )
        assert_json_equals(expected, str(message))
